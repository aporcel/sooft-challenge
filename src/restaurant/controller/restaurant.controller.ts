import { Body, Controller, Get, HttpCode, HttpStatus, Inject, Post, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { FilterDTO } from '../dto/filterDTO';
import { Restaurant } from '../dto/restaurant.dto';
import { RestaurantService } from '../service/restaurant.service';

@Controller('restaurant')
export class RestaurantController {

    constructor(
        private restaurantService:RestaurantService,
    ) {}

    @Get()
    getAllRestaurants(@Query() filterDTO:FilterDTO ): Restaurant[] {
        if(Object.keys(filterDTO).length){
            return this.restaurantService.getRestaurantWithFilter(filterDTO);
        }
        return this.restaurantService.getAllRestaurants();
    }

    @Post()
    createRestaurant(@Body() restaurant:Restaurant): Restaurant {
        return this.restaurantService.createRestaurant(restaurant);
    }
}
