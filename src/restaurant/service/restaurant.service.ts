import { BadGatewayException, BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { FilterDTO } from '../dto/filterDTO';
import { Restaurant } from '../dto/restaurant.dto';

@Injectable()
export class RestaurantService {

        private restaurant_array: Restaurant[] = [];


        getAllRestaurants(): Restaurant[] {
            return this.restaurant_array;
        }

        getRestaurantWithFilter(filterDTO:FilterDTO): Restaurant[]{

            const {kindOfRestaurant} = filterDTO;
            let restaurants= this.getAllRestaurants();

            if(kindOfRestaurant){
                restaurants = restaurants.filter(search => search.kindOfRestaurant  === kindOfRestaurant);
            }
           
            return restaurants;
        }

        createRestaurant(restaurantDTO: Restaurant): Restaurant{

        let restorant= new Restaurant;
            restorant.name= restaurantDTO.name;
            restorant.kindOfRestaurant= restaurantDTO.kindOfRestaurant;
            restorant.songs= restaurantDTO.songs;            
            
       const exist= this.restaurant_array.find(found => found.name==restorant.name);       
       
       if(exist){
           throw new BadRequestException(`Ese nombre de Restaurant ya existe "${restorant.name}", ingrese otro diferente`);
       }
       
       this.restaurant_array.push(restorant);
        return restorant;       
    
    }
}   